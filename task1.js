const jsonfile = require("jsonfile");
const randomstring = require("randomstring");
const got = require("got");

const data = "./input2.json";
const outputFile = "output2.json";

var output = {};
output.result = [];

console.log('loading input file content');

jsonfile.readFile(data,(err, body)=> {

    if(err){
        console.log(err)
    } else {
        console.log(body)

        function reverseString(str){
            let splitString = str.split("");
            let reverseArray = splitString.reverse();
            let joinArray = reverseArray.join("");
            return joinArray;
        }

        for (var i=0; i<body.names.length; i++) {
            console.log("name: " + body.names[i]);

            var outputRes = reverseString(body.names[i]) + randomstring.generate(5) + "@gmail.com";

            console.log("generated email: " + outputRes);
            output.result.push(outputRes);
        }

    jsonfile.writeFile(outputFile, output,{spaces: 2}, function(err) {
    console.log("Emails generated!");
    });
    }
});
